/* (c) Andrzej Wąsowski 2014
 * Replace true constants (besides the very last one) with solutions 
 * to the exercises
 */
package validation

import org.eclipse.emf.ecore.EObject
import T1.Fax

class Constraints {

	// Constraints for the Printer Pool
	
	def static dispatch constraint(T1.PrinterPool it) {
		printerIsMandatory && 
		faxRequiresAPrinter
	}

	def static printerIsMandatory(T1.PrinterPool it) { // example
		(printer != null)
	} 

	def static faxRequiresAPrinter(T1.PrinterPool it) { // task 1.1
		if(it.fax != null)
			return (it.printer != null)
		return true
	} 

	def static dispatch constraint (T1.Fax it) { // task 1.2
		if(it.pool != null)
			return (it.pool.printer != null)
		return true
	} 
	
	
	
	// Constraints for the T2 instances
	// Each Printer pool with a fax, must have a printer, 
	// and each printer pool with a copier must have a scanner and a printer.
	
	def static dispatch constraint (T2.PrinterPool it) { // task 1.3
		// Default return
		var boolean r 		= true
		// Check if there is a fax
		var boolean fax 	= (it.getFax() != null)
		// Check for a printer
		var boolean printer = (it.getPrinter() != null)
		// Check for a copier
		var boolean copier  = (it.getCopier() != null)
		// Check for scanner and printer
		var boolean s_and_p = (it.getScanner() != null && it.getPrinter() != null)
		// There is a printer if there is a fax
		if(fax == true)
			r = printer
		// There are a scanner and a printer if there is a copier
		if(copier == true)
			r = s_and_p
		return r
	}
	
	

	// Constraints for the instances of T3
	// PrinterPool's minimum speed must be 300 lower than its regular speed.
	def static dispatch constraint (T3.PrinterPool it) { // task 1.4
		(it.getMinSpeed()  == (it.getSpeed() - 300))
	}

	// Constraints for the instances of T4
	// Every color printer has a colorPrinterHead.
	def static dispatch constraint (T4.Printer it) { // task 1.5
		if(it.isColor())
			return (it.getHead() != null)
		return true
	}

	// Constraints for the instances of T5
	// A color-capable printer pool contains at least one 
	// color-capable printer.
	def static dispatch constraint (T5.PrinterPool it) { // task 1.6
		if(it.isColor())
			(it.getPrinter() != null && it.getPrinter().exists[color])
	}
	
	// Constraints for the instances of T6
	def static dispatch constraint (T6.PrinterPool it) { 
		// If a Printer pool contains a color scanner, 
		// then it must contain a color printer.
		// task 1.7
		if(it.getScanner().exists[color])
			(it.getPrinter().exists[color])
		
		&& 
		// If a printer pool contains a color scanner, then all 
		// its printers must be color printers.
		// task 1.8
		
		if(it.getScanner().exists[color])
			it.getPrinter().forall[color]

		&&
		
		// There is at most one color printer in any pool
		// task 1.9
		(it.getPrinter().filter[color].size <= 1)
	}
	

	// Catch all case for dynamic dispatch resolution
	def static dispatch boolean constraint(EObject it) {
		true
	}
}
